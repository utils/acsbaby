/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (C) 2015, Amlogic, Inc. All rights reserved.
 * Copyright (C) 2023, Ferass El Hafidi <vitali64pmemail@protonmail.com>
 */
#ifndef TIMING_H_
#define TIMING_H_

/* 
 * This is taken from arch/arm/include/asm/arch-gxb/timing.h in the 
 * Amlogic U-Boot tree. The file is licensed under the GPL-2.0-or-later 
 * license, but since ACSBaby is licensed under the GPL-3.0-or-later license 
 * this file is being used under the terms of the GPL-3.0 license.
 */

#ifdef ACS_TARGET_GXBB
#include <timing_gxbb.h>

typedef struct pll_settings {
	uint16_t cpu_clk;
	uint16_t pxp;
	uint32_t spi_ctrl;
	uint16_t vddee;
	uint16_t vcck;
	uint8_t  szPad[4];
	unsigned long  customer_id;
} __attribute__ ((packed)) pll_t;
#endif
#ifdef ACS_TARGET_GXL
#include <timing_gxl.h>

typedef struct pll_settings {
        uint16_t cpu_clk;
        uint16_t pxp;
        uint32_t spi_ctrl;
        uint16_t vddee;
        uint16_t vcck;
        uint8_t szPad[4];
        unsigned long  customer_id;
        uint16_t  debug_mode;
        uint16_t  ddr_clk_debug;
        uint16_t  cpu_clk_debug;
        uint16_t  rsv_s1;
        /* align */
        uint32_t    ddr_pll_ssc;
        uint32_t    rsv_i1;
        unsigned long   rsv_l3;
        unsigned long   rsv_l4;
        unsigned long   rsv_l5;
} __attribute__ ((packed)) pll_t;
#endif
#endif
