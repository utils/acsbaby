# ACSBaby

*Parse Amlogic ACS settings*

## Contact

You can find me in `#linux-amlogic` on [Libera.Chat](https://libera.chat/).

## Usage

	$ acsbaby [-t acs|bl2|u-boot] [-o offset] file

### Options

**-t type**

* Forces/Sets the image type. Can be either `acs`, `bl2`, `u-boot`, or `u-boot-512`.
  When unset ACSBaby will try to detect which type to use.

**-o offset**

* Sets the offset where the ACS footer is to be found. Mainly useful 
  when dealing with signed U-Boot binaries. A small script is provided too 
  for finding the offset to use.

### Examples

Parse an ACS binary:

	$ acsbaby -o 0x0000a4e0 u-boot.bin


## Compile

Modify `Makefile` to fit your needs, then run:

	$ make

## TODO

* Support other ACS versions

## Known issues

* **Endianness**: Little endian byte order is assumed in the code, thus running 
  on big endian hosts may cause unpredictable misbehaviour.
