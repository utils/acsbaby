/* SPDX-License-Identifier: GPL-3.0-or-later */
/*
 * Copyright (C) 2023, Ferass El Hafidi <vitali64pmemail@protonmail.com>
 */
#ifndef UTIL_H
#define UTIL_H
#include "timing.h"

/* To make gcc happy */
#define __PROBABLY_UNUSED __attribute__((__unused__))

/* Get current date */
const char *get_date();

/* Print data found on ddrs_/ddrt_/pll__ structures */
void print_ddr_settings(ddrs_t);
void print_ddr_timings(ddrt_t [7]);
void print_pll_settings(pll_t);

#endif
