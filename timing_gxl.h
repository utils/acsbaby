/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (C) 2015, Amlogic, Inc. All rights reserved.
 * Copyright (C) 2023, Ferass El Hafidi <vitali64pmemail@protonmail.com>
 */
#ifndef TIMING_GXL_H_
#define TIMING_GXL_H_

typedef struct ddr_settings {
        uint8_t  ddr_channel_set;
        uint8_t  ddr_type;
        uint8_t  ddr_2t_mode;
        uint8_t  ddr_full_test;
        uint8_t  ddr_size_detect;
        uint8_t  ddr_drv;
        uint8_t  ddr_odt;
        uint8_t  ddr_timing_ind;
        uint16_t ddr_size;
        uint16_t ddr_clk;
        uint32_t   ddr_base_addr;
        uint32_t   ddr_start_offset;
        uint32_t   ddr_pll_ctrl;
        uint32_t   ddr_dmc_ctrl;
        uint32_t   ddr0_addrmap[5];
        uint32_t   ddr1_addrmap[5];

        uint32_t   t_pub_ptr[5];  //PUB PTR0-3
        uint16_t t_pub_mr[8];   //PUB MR0-3
        uint32_t   t_pub_odtcr;
        uint32_t   t_pub_dtpr[8]; //PUB DTPR0-3
        uint32_t   t_pub_pgcr0;   //PUB PGCR0
        uint32_t   t_pub_pgcr1;   //PUB PGCR1
        uint32_t   t_pub_pgcr2;   //PUB PGCR2
        uint32_t   t_pub_pgcr3;   //PUB PGCR3
        uint32_t   t_pub_dxccr;   //PUB DXCCR
        uint32_t   t_pub_dtcr0;    //PUB DTCR
        uint32_t   t_pub_dtcr1;    //PUB DTCR
        uint32_t   t_pub_aciocr[5];  //PUB ACIOCRx
        uint32_t   t_pub_dx0gcr[3];  //PUB DX0GCRx
        uint32_t   t_pub_dx1gcr[3];  //PUB DX1GCRx
        uint32_t   t_pub_dx2gcr[3];  //PUB DX2GCRx
        uint32_t   t_pub_dx3gcr[3];  //PUB DX3GCRx
        uint32_t   t_pub_dcr;     //PUB DCR
        uint32_t   t_pub_dtar;
        uint32_t   t_pub_dsgcr;   //PUB DSGCR
        uint32_t   t_pub_zq0pr;   //PUB ZQ0PR
        uint32_t   t_pub_zq1pr;   //PUB ZQ1PR
        uint32_t   t_pub_zq2pr;   //PUB ZQ2PR
        uint32_t   t_pub_zq3pr;   //PUB ZQ3PR
        uint32_t   t_pub_vtcr1;

        /* pctl0 defines */
        uint16_t t_pctl0_1us_pck;   //PCTL TOGCNT1U
        uint16_t t_pctl0_100ns_pck; //PCTL TOGCNT100N
        uint16_t t_pctl0_init_us;   //PCTL TINIT
        uint16_t t_pctl0_rsth_us;   //PCTL TRSTH
        uint32_t   t_pctl0_mcfg;   //PCTL MCFG
        uint32_t   t_pctl0_mcfg1;  //PCTL MCFG1
        uint16_t t_pctl0_scfg;   //PCTL SCFG
        uint16_t t_pctl0_sctl;   //PCTL SCTL
        uint32_t   t_pctl0_ppcfg;
        uint16_t t_pctl0_dfistcfg0;
        uint16_t t_pctl0_dfistcfg1;
        uint16_t t_pctl0_dfitctrldelay;
        uint16_t t_pctl0_dfitphywrdata;
        uint16_t t_pctl0_dfitphywrlta;
        uint16_t t_pctl0_dfitrddataen;
        uint16_t t_pctl0_dfitphyrdlat;
        uint16_t t_pctl0_dfitdramclkdis;
        uint16_t t_pctl0_dfitdramclken;
        uint16_t t_pctl0_dfitphyupdtype0;
        uint16_t t_pctl0_dfitphyupdtype1;
        uint16_t t_pctl0_dfitctrlupdmin;
        uint16_t t_pctl0_dfitctrlupdmax; //TODO - check 16/32
        uint16_t t_pctl0_dfiupdcfg; //TODO - check 16/32
        uint16_t t_pctl0_cmdtstaten;
        uint16_t t_ddr_align;
        uint32_t   t_pctl0_dfiodtcfg;
        uint32_t   t_pctl0_dfiodtcfg1;
        uint32_t   t_pctl0_dfilpcfg0;

        uint32_t   t_pub_acbdlr0; //2015.09.21 CK0 delay for different board PCB design
        uint32_t   t_pub_aclcdlr;

        uint32_t   ddr_func;
        uint8_t  wr_adj_per[6];
        uint8_t  rd_adj_per[6];

        /* 2016.04.12 update */
        uint16_t ddr4_clk; //2016.05.13 add
        uint8_t  ddr4_drv; //2016.05.13 add
        uint8_t  ddr4_odt; //2016.05.13 add

        /* 2016.05.24 update */
        uint32_t  t_pub_acbdlr3;

        /* 2016.07.07 update */
        uint16_t  t_pub_soc_vref_dram_vref;

        /* 2016.12.02 update - lpddr3 */
        uint16_t  t_pub_soc_vref_dram_vref_rank1;
        uint8_t  wr_adj_per_rank1[6];
        uint8_t  rd_adj_per_rank1[6];
} __attribute__ ((packed)) ddrs_t;

typedef struct ddr_timings {
        uint8_t  identifier; //refer ddr.h

        uint8_t  cfg_ddr_rtp;
        uint8_t  cfg_ddr_wtr;
        uint8_t  cfg_ddr_rp;
        uint8_t  cfg_ddr_rcd;
        uint8_t  cfg_ddr_ras;
        uint8_t  cfg_ddr_rrd;
        uint8_t  cfg_ddr_rc;

        uint8_t  cfg_ddr_mrd;
        uint8_t  cfg_ddr_mod;
        uint8_t  cfg_ddr_faw;
        uint8_t  cfg_ddr_wlmrd;
        uint8_t  cfg_ddr_wlo;

        uint8_t  cfg_ddr_xp;

        uint16_t cfg_ddr_rfc;

        uint16_t cfg_ddr_xs;
        uint16_t cfg_ddr_dllk;
        uint8_t  cfg_ddr_cke;
        uint8_t  cfg_ddr_rtodt;
        uint8_t  cfg_ddr_rtw;

        uint8_t  cfg_ddr_refi;
        uint8_t  cfg_ddr_refi_mddr3;
        uint8_t  cfg_ddr_cl;
        uint8_t  cfg_ddr_wr;
        uint8_t  cfg_ddr_cwl;
        uint8_t  cfg_ddr_al;
        uint8_t  cfg_ddr_dqs;
        uint8_t  cfg_ddr_cksre;
        uint8_t  cfg_ddr_cksrx;
        uint8_t  cfg_ddr_zqcs;
        uint8_t  cfg_ddr_xpdll;
        uint16_t cfg_ddr_exsr;
        uint16_t cfg_ddr_zqcl;
        uint16_t cfg_ddr_zqcsi;

        /* aligned */
        /* 2016.04.12 update */
        uint8_t  cfg_ddr_tccdl;
        uint8_t  cfg_ddr_tdqsck;
        uint8_t  cfg_ddr_tdqsckmax;
        uint8_t  rsv_char;

        /* reserved */
        uint32_t   rsv_int;
} __attribute__ ((packed)) ddrt_t;

#endif
