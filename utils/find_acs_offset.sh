#!/bin/sh
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (C) 2023, Ferass El Hafidi <vitali64pmemail@protonmail.com>
# This file is part of ACSBaby.

[ ! -n "$1" ] && \
	printf "Usage: %s file\n" "$0" 1>&2 && \
	exit 1
[ ! -f "$1" ] && \
	printf "%s: %s: file is either invalid or doesn't exist\n" \
	"$0" "$1" 1>&2 && \
	exit 1

# TODO: Find a precise offset.
xxd "$1" | \
	grep "acs__" | \
	awk -F':' '{ print "ACS footer found at offset 0x"$1 }'
