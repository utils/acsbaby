/* SPDX-License-Identifier: GPL-3.0-or-later */
/*
 * Copyright (C) 2023, Ferass El Hafidi <vitali64pmemail@protonmail.com>
 */
#ifndef PARSE_H
#define PARSE_H
#include "acsbaby.h"
#include "timing.h"

/* Parse ACS footer */
acs_t parse_acs_footer(const int, const uint32_t);

ddrs_t parse_ddrs(const int, const acs_t, const uint32_t);
void   parse_ddrt(const int, const acs_t, const uint32_t, ddrt_t [7]);
pll_t  parse_pll(const int, const acs_t, const uint32_t);

#endif
