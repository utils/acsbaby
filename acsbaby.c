/* SPDX-License-Identifier: GPL-3.0-or-later */
/*
 * Copyright (C) 2023-2025, Ferass El Hafidi <vitali64pmemail@protonmail.com>
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <err.h>

#include "acsbaby.h"
#include "parse.h"
#include "timing.h"
#include "util.h"

int print_usage(char *);

int main(int argc, char *argv[]) {
	int argument, fildes;
	uint16_t acs_offset;
	uint32_t diff_offset;
	char buffer[4096], *type = "unspecified", *argv0 = strdup(argv[0]);
	while ((argument = getopt(argc, argv, "t:o:")) != -1) {
		if (argument == '?') return print_usage(argv0);
		else if (argument == 't' && optarg != NULL) {
			if (strcmp("u-boot", optarg) && 
				strcmp("u-boot-512", optarg) &&
				strcmp("acs", optarg) &&
				strcmp("bl2", optarg))
				return print_usage(argv0);
			type = strdup(optarg);
		}
		else if (argument == 'o' && optarg != NULL) {
			/* Check if it's a hexadecimal (prefixed by '0x') */
			if (strncmp("0x", optarg, 2))
				return print_usage(argv0);
			acs_offset = (uint16_t)strtol(optarg, NULL, 16);
		}
	} argc -= optind; argv += optind;

	if (argc < 1) return print_usage(argv0);
	if (!acs_offset) errx(EINVAL, "you need to specify the offset");

	/* Open the file */
	fildes = open(argv[0], O_RDONLY);
	if (fildes == -1) err(errno, "can't open file");

	if (!strcmp(type, "unspecified")) {
		/*
		 * Try figuring out the binary we have if it isn't 
		 * explicitely set.
		 */
		if (read(fildes, &buffer, 533) == -1)
			err(errno, "%s", "can't read");
		/* Check for the "@AML" magic word (in a signed gxbb-eMMC u-boot.bin) */
		if (buffer[16] == '@' && 
				buffer[17] == 'A' && 
				buffer[18] == 'M' && 
				buffer[19] == 'L')
			type = "u-boot";
		/* Or in a u-boot.bin with offset */
		else if (buffer[528] == '@' && 
				buffer[529] == 'A' && 
				buffer[530] == 'M' && 
				buffer[531] == 'L')
			type = "u-boot-512";
		/* Check for the "Built :" string (in acs.bin) */
		else if (buffer[16] == 'B' &&
				buffer[17] == 'u' &&
				buffer[18] == 'i' &&
				buffer[19] == 'l' &&
				buffer[20] == 't' &&
				buffer[21] == ' ' &&
				buffer[22] == ':')
			type = "acs";
		/* Assume it's a BL2 binary */
		else	type = "bl2";
		printf("// Binary type: %s\n", type);
	}

	acs_t acs_footer = parse_acs_footer(fildes, acs_offset);

	printf(
		"/* acs.c\n"
		" * ACSBaby: Parse Amlogic ACS.bin - https://git.vitali64.duckdns.org/utils/acsbaby.git\n"
		" * File: %s - Type: %s\n"
		" */\n"
		"#include <asm/arch/acs.h>\n"
		"#include <asm/arch/timing.h>\n"
		"#include \"timing.c\"\n\n"
		"struct acs_setting __acs_set = {\n"
		"\t.acs_magic        = \"acs__\",\n"
		"\t.chip_type        = 0x%x,\n"
		"\t.version          = %d,\n"
		"\t.acs_set_length   = 0x%x,\n\n"
		/* "ddrs_" entry */
		"\t.ddr_magic        = \"ddrs_\",\n"
		"\t.ddr_set_version  = %d,\n"
		"\t.ddr_set_length   = 0x%x,\n"
		"\t.ddr_set_addr     = (unsigned long)"
			"(&__ddr_setting), /* 0x%x */\n\n"
		/* "ddrt_" entry */
		"\t.ddrt_magic       = \"ddrt_\",\n"
		"\t.ddrt_set_version = %d,\n"
		"\t.ddrt_set_length  = 0x%x,\n"
		"\t.ddrt_set_addr    = (unsigned long)"
			"(&__ddr_timming), /* 0x%x */\n\n"
		/* "pll__" entry */
		"\t.pll_magic        = \"pll__\",\n"
		"\t.pll_set_version  = %d,\n"
		"\t.pll_set_length   = 0x%x,\n"
		"\t.pll_set_addr     = (unsigned long)"
			"(&__pll_setting), /* 0x%x */\n};\n",
		argv[0], /* File name */
		type,    /* File type */

		acs_footer.chip_type,
		acs_footer.version,
		acs_footer.size, 
		
		acs_footer.ddrs.version,
		acs_footer.ddrs.size, 
		acs_footer.ddrs.address, 
		
		acs_footer.ddrt.version,
		acs_footer.ddrt.size, 
		acs_footer.ddrt.address, 
		
		acs_footer.pll.version,
		acs_footer.pll.size,
		acs_footer.pll.address
	);
	printf(
		"/* timing.c\n"
		" * ACSBaby: Parse Amlogic ACS.bin - https://git.vitali64.duckdns.org/utils/acsbaby.git\n"
		" * File: %s - Type: %s\n"
		" */\n"
		"#include <asm/arch/timing.h>\n"
		"#include <asm/arch/ddr_define.h>\n", argv[0], type
	);
	if (!strcmp("acs", type))
		diff_offset = 0;
	else if (!strcmp("bl2", type))
		diff_offset = 0xd9001000; /* BL2 base address */
	else if (!strcmp("u-boot", type))
		diff_offset = 0xd9000000; /* BL2 @AML load address */
	else if (!strcmp("u-boot-512", type))
		/* u-boot.bin with 0x200 offset added */
		diff_offset = 0xd8fffe00;

	ddrs_t ddr_settings = parse_ddrs(fildes, acs_footer, diff_offset);
	ddrt_t ddr_timings[7];
	parse_ddrt(fildes, acs_footer, diff_offset, ddr_timings);
	pll_t pll_settings = parse_pll(fildes, acs_footer, diff_offset);
	
	print_ddr_settings(ddr_settings);
	print_ddr_timings(ddr_timings);
	print_pll_settings(pll_settings);

	if (close(fildes) != 0)
		err(errno, "%s", "can't close file descriptor");

	return 0;
}

int print_usage(char *name) {
	printf(
		"ACSBaby: Parse Amlogic ACS.bin - https://git.vitali64.duckdns.org/utils/acsbaby.git\n"
		"Built for: %s\n"
		"Usage: %s [-t acs|bl2|u-boot] -o offset file\n",
#if ACS_TARGET_GXBB
		"gxbb",
#elif ACS_TARGET_GXL
		"gxl",
#else
		/* Should *not* happen */
		"invalid",
#endif
		name
	);
	return EINVAL;
}
