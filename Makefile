# SPDX-License-Identifier: GPL-3.0-of-later
#
# Copyright (C) 2023, Ferass El Hafidi <vitali64pmemail@protonmail.com>
.POSIX:

#
# Usual compiler configuration
#
ACS_TARGET=GXL # GXL or GXBB
CC=cc
CFLAGS=-D_POSIX_C_SOURCE=200809L -DACS_TARGET_$(ACS_TARGET) -I. -Wall -Wextra -Werror -g -pedantic -std=c99
NOLINKER=-c
SRC=\
    	util\
	parse\
	acsbaby

# Targets
all: clean compile

compile:
	for src in $(SRC); do \
		$(CC) $(CFLAGS) $(NOLINKER) $${src}.c -o $${src}.o; done
	$(CC) $(CFLAGS) $(SRC:=.o) -o acsbaby

clean:
	rm -Rf acsbaby *.o
