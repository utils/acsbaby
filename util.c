/* SPDX-License-Identifier: GPL-3.0-or-later */
/*
 * Copyright (C) 2023, Ferass El Hafidi <vitali64pmemail@protonmail.com>
 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "timing.h"
#include "util.h"

/* Gotta improve that... */
void print_ddr_settings(ddrs_t ddr_settings __PROBABLY_UNUSED) {
	printf(
		"\n/* \"ddrs_\" structure */\n"
		"ddr_set_t __ddr_setting = {\n"
#if ACS_TARGET_GXBB
		"\t.ddr_channel_set          = 0x%x,\n"
		"\t.ddr_type                 = 0x%x,\n"
		"\t.ddr_2t_mode              = 0x%x,\n"
		"\t.ddr_full_test            = 0x%x,\n"
		"\t.ddr_size_detect          = 0x%x,\n"
		"\t.ddr_drv                  = 0x%x,\n"
		"\t.ddr_odt                  = 0x%x,\n"
		"\t.ddr_timing_ind           = 0x%x,\n"
		"\t.ddr_size                 = 0x%x,\n"
		"\t.ddr_clk                  = 0x%x,\n"
		"\t.ddr_base_addr            = 0x%x,\n"
		"\t.ddr_start_offset         = 0x%x,\n"
		"\t.ddr_pll_ctrl             = 0x%x,\n"
		"\t.ddr_dmc_ctrl             = 0x%x,\n"
		"\t.ddr0_addrmap[0]          = 0x%x,\n"
		"\t.ddr0_addrmap[1]          = 0x%x,\n"
		"\t.ddr0_addrmap[2]          = 0x%x,\n"
		"\t.ddr0_addrmap[3]          = 0x%x,\n"
		"\t.ddr0_addrmap[4]          = 0x%x,\n"
		"\t.ddr1_addrmap[0]          = 0x%x,\n"
		"\t.ddr1_addrmap[1]          = 0x%x,\n"
		"\t.ddr1_addrmap[2]          = 0x%x,\n"
		"\t.ddr1_addrmap[3]          = 0x%x,\n"
		"\t.ddr1_addrmap[4]          = 0x%x,\n\n"
		"\t.t_pub_ptr[0]             = 0x%x,\n"
		"\t.t_pub_ptr[1]             = 0x%x,\n"
		"\t.t_pub_ptr[2]             = 0x%x,\n"
		"\t.t_pub_ptr[3]             = 0x%x,\n"
		"\t.t_pub_ptr[4]             = 0x%x,\n"
		"\t.t_pub_mr[0]              = 0x%x,\n"
		"\t.t_pub_mr[1]              = 0x%x,\n"
		"\t.t_pub_mr[2]              = 0x%x,\n"
		"\t.t_pub_mr[3]              = 0x%x,\n"
		"\t.t_pub_odtcr              = 0x%x,\n"
		"\t.t_pub_dtpr[0]            = 0x%x,\n"
		"\t.t_pub_dtpr[1]            = 0x%x,\n"
		"\t.t_pub_dtpr[2]            = 0x%x,\n"
		"\t.t_pub_dtpr[3]            = 0x%x,\n"
		"\t.t_pub_pgcr0              = 0x%x,\n"
		"\t.t_pub_pgcr1              = 0x%x,\n"
		"\t.t_pub_pgcr2              = 0x%x,\n"
		"\t.t_pub_pgcr3              = 0x%x,\n"
		"\t.t_pub_dxccr              = 0x%x,\n"
		"\t.t_pub_dtcr               = 0x%x,\n"
		"\t.t_pub_aciocr[0]          = 0x%x,\n"
		"\t.t_pub_aciocr[1]          = 0x%x,\n"
		"\t.t_pub_aciocr[2]          = 0x%x,\n"
		"\t.t_pub_aciocr[3]          = 0x%x,\n"
		"\t.t_pub_aciocr[4]          = 0x%x,\n"
		"\t.t_pub_dx0gcr[0]          = 0x%x,\n"
		"\t.t_pub_dx0gcr[1]          = 0x%x,\n"
		"\t.t_pub_dx0gcr[2]          = 0x%x,\n"
		"\t.t_pub_dx1gcr[0]          = 0x%x,\n"
		"\t.t_pub_dx1gcr[1]          = 0x%x,\n"
		"\t.t_pub_dx1gcr[2]          = 0x%x,\n"
		"\t.t_pub_dx2gcr[0]          = 0x%x,\n"
		"\t.t_pub_dx2gcr[1]          = 0x%x,\n"
		"\t.t_pub_dx2gcr[2]          = 0x%x,\n"
		"\t.t_pub_dx3gcr[0]          = 0x%x,\n"
		"\t.t_pub_dx3gcr[1]          = 0x%x,\n"
		"\t.t_pub_dx3gcr[2]          = 0x%x,\n"
		"\t.t_pub_dcr                = 0x%x,\n"
		"\t.t_pub_dtar               = 0x%x,\n"
		"\t.t_pub_dsgcr              = 0x%x,\n"
		"\t.t_pub_zq0pr              = 0x%x,\n"
		"\t.t_pub_zq1pr              = 0x%x,\n"
		"\t.t_pub_zq2pr              = 0x%x,\n"
		"\t.t_pub_zq3pr              = 0x%x,\n\n"
		"\t.t_pctl0_1us_pck          = 0x%x,\n"
		"\t.t_pctl0_100ns_pck        = 0x%x,\n"
		"\t.t_pctl0_init_us          = 0x%x,\n"
		"\t.t_pctl0_rsth_us          = 0x%x,\n"
		"\t.t_pctl0_mcfg             = 0x%x,\n"
		"\t.t_pctl0_mcfg1            = 0x%x,\n"
		"\t.t_pctl0_scfg             = 0x%x,\n"
		"\t.t_pctl0_sctl             = 0x%x,\n"
		"\t.t_pctl0_ppcfg            = 0x%x,\n"
		"\t.t_pctl0_dfistcfg0        = 0x%x,\n"
		"\t.t_pctl0_dfistcfg1        = 0x%x,\n"
		"\t.t_pctl0_dfitctrldelay    = 0x%x,\n"
		"\t.t_pctl0_dfitphywrdata    = 0x%x,\n"
		"\t.t_pctl0_dfitphywrlta     = 0x%x,\n"
		"\t.t_pctl0_dfitrddataen     = 0x%x,\n"
		"\t.t_pctl0_dfitphyrdlat     = 0x%x,\n"
		"\t.t_pctl0_dfitdramclkdis   = 0x%x,\n"
		"\t.t_pctl0_dfitdramclken    = 0x%x,\n"
		"\t.t_pctl0_dfitphyupdtype1  = 0x%x,\n"
		"\t.t_pctl0_dfitctrlupdmin   = 0x%x,\n"
		"\t.t_pctl0_cmdtstaten       = 0x%x,\n"
		"\t.t_pctl0_dfiodtcfg        = 0x%x,\n"
		"\t.t_pctl0_dfiodtcfg1       = 0x%x,\n"
		"\t.t_pctl0_dfilpcfg0        = 0x%x,\n\n"
		"\t.t_pub_acbdlr0            = 0x%x,\n\n"
		"\t.ddr_func                 = 0x%x,\n"
		"\t.wr_adj_per[0]            = 0x%x,\n"
		"\t.wr_adj_per[1]            = 0x%x,\n"
		"\t.wr_adj_per[2]            = 0x%x,\n"
		"\t.wr_adj_per[3]            = 0x%x,\n"
		"\t.wr_adj_per[4]            = 0x%x,\n"
		"\t.wr_adj_per[5]            = 0x%x,\n"
		"\t.rd_adj_per[0]            = 0x%x,\n"
		"\t.rd_adj_per[1]            = 0x%x,\n"
		"\t.rd_adj_per[2]            = 0x%x,\n"
		"\t.rd_adj_per[3]            = 0x%x,\n"
		"\t.rd_adj_per[4]            = 0x%x,\n"
		"\t.rd_adj_per[5]            = 0x%x,\n"
		"\t.t_pub_mr11               = 0x%x,\n"
		"\t.t_lpddr3_ca0             = 0x%x,\n"
		"\t.t_lpddr3_ca1             = 0x%x,\n"
		"\t.t_lpddr3_remap           = 0x%x,\n"
		"\t.t_lpddr3_wl              = 0x%x,\n"
		"\t.rsv1                     = 0x%x,\n"
		"\t.rsv2                     = 0x%x\n"
		"};\n",
		ddr_settings.ddr_channel_set,
		ddr_settings.ddr_type,
		ddr_settings.ddr_2t_mode,
		ddr_settings.ddr_full_test,
		ddr_settings.ddr_size_detect,
		ddr_settings.ddr_drv,
		ddr_settings.ddr_odt,
		ddr_settings.ddr_timing_ind,
		ddr_settings.ddr_size,
		ddr_settings.ddr_clk,
		ddr_settings.ddr_base_addr,
		ddr_settings.ddr_start_offset,
		ddr_settings.ddr_pll_ctrl,
		ddr_settings.ddr_dmc_ctrl,
		ddr_settings.ddr0_addrmap[0],
		ddr_settings.ddr0_addrmap[1],
		ddr_settings.ddr0_addrmap[2],
		ddr_settings.ddr0_addrmap[3],
		ddr_settings.ddr0_addrmap[4],
		ddr_settings.ddr1_addrmap[0],
		ddr_settings.ddr1_addrmap[1],
		ddr_settings.ddr1_addrmap[2],
		ddr_settings.ddr1_addrmap[3],
		ddr_settings.ddr1_addrmap[4],

		ddr_settings.t_pub_ptr[0],
		ddr_settings.t_pub_ptr[1],
		ddr_settings.t_pub_ptr[2],
		ddr_settings.t_pub_ptr[3],
		ddr_settings.t_pub_ptr[4],
		ddr_settings.t_pub_mr[0],
		ddr_settings.t_pub_mr[1],
		ddr_settings.t_pub_mr[2],
		ddr_settings.t_pub_mr[3],
		ddr_settings.t_pub_odtcr,
		ddr_settings.t_pub_dtpr[0],
		ddr_settings.t_pub_dtpr[1],
		ddr_settings.t_pub_dtpr[2],
		ddr_settings.t_pub_dtpr[3],
		ddr_settings.t_pub_pgcr0,
		ddr_settings.t_pub_pgcr1,
		ddr_settings.t_pub_pgcr2,
		ddr_settings.t_pub_pgcr3,
		ddr_settings.t_pub_dxccr,
		ddr_settings.t_pub_dtcr,
		ddr_settings.t_pub_aciocr[0],
		ddr_settings.t_pub_aciocr[1],
		ddr_settings.t_pub_aciocr[2],
		ddr_settings.t_pub_aciocr[3],
		ddr_settings.t_pub_aciocr[4],
		ddr_settings.t_pub_dx0gcr[0],
		ddr_settings.t_pub_dx0gcr[1],
		ddr_settings.t_pub_dx0gcr[2],
		ddr_settings.t_pub_dx1gcr[0],
		ddr_settings.t_pub_dx1gcr[1],
		ddr_settings.t_pub_dx1gcr[2],
		ddr_settings.t_pub_dx2gcr[0],
		ddr_settings.t_pub_dx2gcr[1],
		ddr_settings.t_pub_dx2gcr[2],
		ddr_settings.t_pub_dx3gcr[0],
		ddr_settings.t_pub_dx3gcr[1],
		ddr_settings.t_pub_dx3gcr[2],
		ddr_settings.t_pub_dcr,
		ddr_settings.t_pub_dtar,
		ddr_settings.t_pub_dsgcr,
		ddr_settings.t_pub_zq0pr,
		ddr_settings.t_pub_zq1pr,
		ddr_settings.t_pub_zq2pr,
		ddr_settings.t_pub_zq3pr,

		ddr_settings.t_pctl0_1us_pck,
		ddr_settings.t_pctl0_100ns_pck,
		ddr_settings.t_pctl0_init_us,
		ddr_settings.t_pctl0_rsth_us,
		ddr_settings.t_pctl0_mcfg,
		ddr_settings.t_pctl0_mcfg1,
		ddr_settings.t_pctl0_scfg,
		ddr_settings.t_pctl0_sctl,
		ddr_settings.t_pctl0_ppcfg,
		ddr_settings.t_pctl0_dfistcfg0,
		ddr_settings.t_pctl0_dfistcfg1,
		ddr_settings.t_pctl0_dfitctrldelay,
		ddr_settings.t_pctl0_dfitphywrdata,
		ddr_settings.t_pctl0_dfitphywrlta,
		ddr_settings.t_pctl0_dfitrddataen,
		ddr_settings.t_pctl0_dfitphyrdlat,
		ddr_settings.t_pctl0_dfitdramclkdis,
		ddr_settings.t_pctl0_dfitdramclken,
		ddr_settings.t_pctl0_dfitphyupdtype1,
		ddr_settings.t_pctl0_dfitctrlupdmin,
		ddr_settings.t_pctl0_cmdtstaten,
		ddr_settings.t_pctl0_dfiodtcfg,
		ddr_settings.t_pctl0_dfiodtcfg1,
		ddr_settings.t_pctl0_dfilpcfg0,

		ddr_settings.t_pub_acbdlr0,

		ddr_settings.ddr_func,
		ddr_settings.wr_adj_per[0],
		ddr_settings.wr_adj_per[1],
		ddr_settings.wr_adj_per[2],
		ddr_settings.wr_adj_per[3],
		ddr_settings.wr_adj_per[4],
		ddr_settings.wr_adj_per[5],
		ddr_settings.rd_adj_per[0],
		ddr_settings.rd_adj_per[1],
		ddr_settings.rd_adj_per[2],
		ddr_settings.rd_adj_per[3],
		ddr_settings.rd_adj_per[4],
		ddr_settings.rd_adj_per[5],
		ddr_settings.t_pub_mr11,
		ddr_settings.t_lpddr3_ca0,
		ddr_settings.t_lpddr3_ca1,
		ddr_settings.t_lpddr3_remap,
		ddr_settings.t_lpddr3_wl,
		ddr_settings.rsv1,
		ddr_settings.rsv2
#elif ACS_TARGET_GXL
		"\t\t.ddr_channel_set        = 0x%x\n"
		"\t\t.ddr_type               = 0x%x\n"
		"\t\t.ddr_2t_mode            = 0x%x\n"
		"\t\t.ddr_full_test          = 0x%x\n"
		"\t\t.ddr_size_detect        = 0x%x\n"
		"\t\t.ddr_drv                = 0x%x\n"
		"\t\t.ddr_odt                = 0x%x\n"
		"\t\t.ddr_timing_ind         = 0x%x\n"
		"\t\t.ddr_size               = 0x%x\n"
		"\t\t.ddr_clk                = 0x%x\n"
		"\t\t.ddr_base_addr          = 0x%x\n"
		"\t\t.ddr_start_offset       = 0x%x\n"
		"\t\t.ddr_pll_ctrl           = 0x%x\n"
		"\t\t.ddr_dmc_ctrl           = 0x%x\n"
		"\t\t.ddr0_addrmap[0]        = 0x%x\n"
		"\t\t.ddr0_addrmap[1]        = 0x%x\n"
		"\t\t.ddr0_addrmap[2]        = 0x%x\n"
		"\t\t.ddr0_addrmap[3]        = 0x%x\n"
		"\t\t.ddr0_addrmap[4]        = 0x%x\n"
		"\t\t.ddr1_addrmap[0]        = 0x%x\n"
		"\t\t.ddr1_addrmap[1]        = 0x%x\n"
		"\t\t.ddr1_addrmap[2]        = 0x%x\n"
		"\t\t.ddr1_addrmap[3]        = 0x%x\n"
		"\t\t.ddr1_addrmap[4]        = 0x%x\n"

		"\t\t.t_pub_ptr[0]           = 0x%x\n"
		"\t\t.t_pub_ptr[1]           = 0x%x\n"
		"\t\t.t_pub_ptr[2]           = 0x%x\n"
		"\t\t.t_pub_ptr[3]           = 0x%x\n"
		"\t\t.t_pub_ptr[4]           = 0x%x\n"
		"\t\t.t_pub_mr[0]            = 0x%x\n"   
		"\t\t.t_pub_mr[1]            = 0x%x\n"   
		"\t\t.t_pub_mr[2]            = 0x%x\n"   
		"\t\t.t_pub_mr[3]            = 0x%x\n"   
		"\t\t.t_pub_mr[4]            = 0x%x\n"   
		"\t\t.t_pub_mr[5]            = 0x%x\n"   
		"\t\t.t_pub_mr[6]            = 0x%x\n"   
		"\t\t.t_pub_mr[7]            = 0x%x\n"   
		"\t\t.t_pub_odtcr            = 0x%x\n"
		"\t\t.t_pub_dtpr[0]          = 0x%x\n" 
		"\t\t.t_pub_dtpr[1]          = 0x%x\n" 
		"\t\t.t_pub_dtpr[2]          = 0x%x\n" 
		"\t\t.t_pub_dtpr[3]          = 0x%x\n" 
		"\t\t.t_pub_dtpr[4]          = 0x%x\n" 
		"\t\t.t_pub_dtpr[5]          = 0x%x\n" 
		"\t\t.t_pub_dtpr[6]          = 0x%x\n" 
		"\t\t.t_pub_dtpr[7]          = 0x%x\n" 
		"\t\t.t_pub_pgcr0            = 0x%x\n"   
		"\t\t.t_pub_pgcr1            = 0x%x\n"   
		"\t\t.t_pub_pgcr2            = 0x%x\n"   
		"\t\t.t_pub_pgcr3            = 0x%x\n"   
		"\t\t.t_pub_dxccr            = 0x%x\n"   
		"\t\t.t_pub_dtcr0            = 0x%x\n"    
		"\t\t.t_pub_dtcr1            = 0x%x\n"    
		"\t\t.t_pub_aciocr[0]        = 0x%x\n"  
		"\t\t.t_pub_aciocr[1]        = 0x%x\n"  
		"\t\t.t_pub_aciocr[2]        = 0x%x\n"  
		"\t\t.t_pub_aciocr[3]        = 0x%x\n"  
		"\t\t.t_pub_aciocr[4]        = 0x%x\n"

		"\t\t.t_pub_dx0gcr[0]        = 0x%x\n"  
		"\t\t.t_pub_dx0gcr[1]        = 0x%x\n"  
		"\t\t.t_pub_dx0gcr[2]        = 0x%x\n"  
		"\t\t.t_pub_dx1gcr[0]        = 0x%x\n"  
		"\t\t.t_pub_dx1gcr[1]        = 0x%x\n"  
		"\t\t.t_pub_dx1gcr[2]        = 0x%x\n"  
		"\t\t.t_pub_dx2gcr[0]        = 0x%x\n"  
		"\t\t.t_pub_dx2gcr[1]        = 0x%x\n"  
		"\t\t.t_pub_dx2gcr[2]        = 0x%x\n"  
		"\t\t.t_pub_dx3gcr[0]        = 0x%x\n"  
		"\t\t.t_pub_dx3gcr[1]        = 0x%x\n"  
		"\t\t.t_pub_dx3gcr[2]        = 0x%x\n"  
		"\t\t.t_pub_dcr              = 0x%x\n"     
		"\t\t.t_pub_dtar             = 0x%x\n"
		"\t\t.t_pub_dsgcr            = 0x%x\n"   
		"\t\t.t_pub_zq0pr            = 0x%x\n"   
		"\t\t.t_pub_zq1pr            = 0x%x\n"   
		"\t\t.t_pub_zq2pr            = 0x%x\n"   
		"\t\t.t_pub_zq3pr            = 0x%x\n"   
		"\t\t.t_pub_vtcr1            = 0x%x\n"

		"\t\t.t_pctl0_1us_pck        = 0x%x\n"   
		"\t\t.t_pctl0_100ns_pck      = 0x%x\n" 
		"\t\t.t_pctl0_init_us        = 0x%x\n"   
		"\t\t.t_pctl0_rsth_us        = 0x%x\n"   
		"\t\t.t_pctl0_mcfg           = 0x%x\n"   
		"\t\t.t_pctl0_mcfg1          = 0x%x\n"  
		"\t\t.t_pctl0_scfg           = 0x%x\n"   
		"\t\t.t_pctl0_sctl           = 0x%x\n"   
		"\t\t.t_pctl0_ppcfg          = 0x%x\n"
		"\t\t.t_pctl0_dfistcfg0      = 0x%x\n"
		"\t\t.t_pctl0_dfistcfg1      = 0x%x\n"
		"\t\t.t_pctl0_dfitctrldelay  = 0x%x\n"
		"\t\t.t_pctl0_dfitphywrdata  = 0x%x\n"
		"\t\t.t_pctl0_dfitphywrlta   = 0x%x\n"
		"\t\t.t_pctl0_dfitrddataen   = 0x%x\n"
		"\t\t.t_pctl0_dfitphyrdlat   = 0x%x\n"
		"\t\t.t_pctl0_dfitdramclkdis = 0x%x\n"
		"\t\t.t_pctl0_dfitdramclken  = 0x%x\n"
		"\t\t.t_pctl0_dfitphyupdtype0= 0x%x\n"
		"\t\t.t_pctl0_dfitphyupdtype1= 0x%x\n"
		"\t\t.t_pctl0_dfitctrlupdmin = 0x%x\n"
		"\t\t.t_pctl0_dfitctrlupdmax = 0x%x\n" 
		"\t\t.t_pctl0_dfiupdcfg      = 0x%x\n" 
		"\t\t.t_pctl0_cmdtstaten     = 0x%x\n"
		"\t\t.t_ddr_align            = 0x%x\n"
		"\t\t.t_pctl0_dfiodtcfg      = 0x%x\n"
		"\t\t.t_pctl0_dfiodtcfg1     = 0x%x\n"
		"\t\t.t_pctl0_dfilpcfg0      = 0x%x\n"

		"\t\t.t_pub_acbdlr0          = 0x%x\n" 
		"\t\t.t_pub_aclcdlr          = 0x%x\n"

		"\t\t.ddr_func               = 0x%x\n"
		"\t\t.wr_adj_per[0]          = 0x%x\n"
		"\t\t.wr_adj_per[1]          = 0x%x\n"
		"\t\t.wr_adj_per[2]          = 0x%x\n"
		"\t\t.wr_adj_per[3]          = 0x%x\n"
		"\t\t.wr_adj_per[4]          = 0x%x\n"
		"\t\t.wr_adj_per[5]          = 0x%x\n"
		"\t\t.rd_adj_per[0]          = 0x%x\n"
		"\t\t.rd_adj_per[1]          = 0x%x\n"
		"\t\t.rd_adj_per[2]          = 0x%x\n"
		"\t\t.rd_adj_per[3]          = 0x%x\n"
		"\t\t.rd_adj_per[4]          = 0x%x\n"
		"\t\t.rd_adj_per[5]          = 0x%x\n",

		ddr_settings.ddr_channel_set,
		ddr_settings.ddr_type,
		ddr_settings.ddr_2t_mode,
		ddr_settings.ddr_full_test,
		ddr_settings.ddr_size_detect,
		ddr_settings.ddr_drv,
		ddr_settings.ddr_odt,
		ddr_settings.ddr_timing_ind,
		ddr_settings.ddr_size,
		ddr_settings.ddr_clk,
		ddr_settings.ddr_base_addr,
		ddr_settings.ddr_start_offset,
		ddr_settings.ddr_pll_ctrl,
		ddr_settings.ddr_dmc_ctrl,
		ddr_settings.ddr0_addrmap[0],
		ddr_settings.ddr0_addrmap[1],
		ddr_settings.ddr0_addrmap[2],
		ddr_settings.ddr0_addrmap[3],
		ddr_settings.ddr0_addrmap[4],
		ddr_settings.ddr1_addrmap[0],
		ddr_settings.ddr1_addrmap[1],
		ddr_settings.ddr1_addrmap[2],
		ddr_settings.ddr1_addrmap[3],
		ddr_settings.ddr1_addrmap[4],

		ddr_settings.t_pub_ptr[0],
		ddr_settings.t_pub_ptr[1],
		ddr_settings.t_pub_ptr[2],
		ddr_settings.t_pub_ptr[3],
		ddr_settings.t_pub_ptr[4],
		ddr_settings.t_pub_mr[0],   
		ddr_settings.t_pub_mr[1],   
		ddr_settings.t_pub_mr[2],   
		ddr_settings.t_pub_mr[3],   
		ddr_settings.t_pub_mr[4],   
		ddr_settings.t_pub_mr[5],   
		ddr_settings.t_pub_mr[6],   
		ddr_settings.t_pub_mr[7],   
		ddr_settings.t_pub_odtcr,
		ddr_settings.t_pub_dtpr[0], 
		ddr_settings.t_pub_dtpr[1], 
		ddr_settings.t_pub_dtpr[2], 
		ddr_settings.t_pub_dtpr[3], 
		ddr_settings.t_pub_dtpr[4], 
		ddr_settings.t_pub_dtpr[5], 
		ddr_settings.t_pub_dtpr[6], 
		ddr_settings.t_pub_dtpr[7], 
		ddr_settings.t_pub_pgcr0,   
		ddr_settings.t_pub_pgcr1,   
		ddr_settings.t_pub_pgcr2,   
		ddr_settings.t_pub_pgcr3,   
		ddr_settings.t_pub_dxccr,   
		ddr_settings.t_pub_dtcr0,    
		ddr_settings.t_pub_dtcr1,    
		ddr_settings.t_pub_aciocr[0],  
		ddr_settings.t_pub_aciocr[1],  
		ddr_settings.t_pub_aciocr[2],  
		ddr_settings.t_pub_aciocr[3],  
		ddr_settings.t_pub_aciocr[4],

		ddr_settings.t_pub_dx0gcr[0],  
		ddr_settings.t_pub_dx0gcr[1],  
		ddr_settings.t_pub_dx0gcr[2],  
		ddr_settings.t_pub_dx1gcr[0],  
		ddr_settings.t_pub_dx1gcr[1],  
		ddr_settings.t_pub_dx1gcr[2],  
		ddr_settings.t_pub_dx2gcr[0],  
		ddr_settings.t_pub_dx2gcr[1],  
		ddr_settings.t_pub_dx2gcr[2],  
		ddr_settings.t_pub_dx3gcr[0],  
		ddr_settings.t_pub_dx3gcr[1],  
		ddr_settings.t_pub_dx3gcr[2],  
		ddr_settings.t_pub_dcr,     
		ddr_settings.t_pub_dtar,
		ddr_settings.t_pub_dsgcr,   
		ddr_settings.t_pub_zq0pr,   
		ddr_settings.t_pub_zq1pr,   
		ddr_settings.t_pub_zq2pr,   
		ddr_settings.t_pub_zq3pr,   
		ddr_settings.t_pub_vtcr1,

		ddr_settings.t_pctl0_1us_pck,   
		ddr_settings.t_pctl0_100ns_pck, 
		ddr_settings.t_pctl0_init_us,   
		ddr_settings.t_pctl0_rsth_us,   
		ddr_settings.t_pctl0_mcfg,   
		ddr_settings.t_pctl0_mcfg1,  
		ddr_settings.t_pctl0_scfg,   
		ddr_settings.t_pctl0_sctl,   
		ddr_settings.t_pctl0_ppcfg,
		ddr_settings.t_pctl0_dfistcfg0,
		ddr_settings.t_pctl0_dfistcfg1,
		ddr_settings.t_pctl0_dfitctrldelay,
		ddr_settings.t_pctl0_dfitphywrdata,
		ddr_settings.t_pctl0_dfitphywrlta,
		ddr_settings.t_pctl0_dfitrddataen,
		ddr_settings.t_pctl0_dfitphyrdlat,
		ddr_settings.t_pctl0_dfitdramclkdis,
		ddr_settings.t_pctl0_dfitdramclken,
		ddr_settings.t_pctl0_dfitphyupdtype0,
		ddr_settings.t_pctl0_dfitphyupdtype1,
		ddr_settings.t_pctl0_dfitctrlupdmin,
		ddr_settings.t_pctl0_dfitctrlupdmax, 
		ddr_settings.t_pctl0_dfiupdcfg, 
		ddr_settings.t_pctl0_cmdtstaten,
		ddr_settings.t_ddr_align,
		ddr_settings.t_pctl0_dfiodtcfg,
		ddr_settings.t_pctl0_dfiodtcfg1,
		ddr_settings.t_pctl0_dfilpcfg0,

		ddr_settings.t_pub_acbdlr0, 
		ddr_settings.t_pub_aclcdlr,

		ddr_settings.ddr_func,
		ddr_settings.wr_adj_per[0],
		ddr_settings.wr_adj_per[1],
		ddr_settings.wr_adj_per[2],
		ddr_settings.wr_adj_per[3],
		ddr_settings.wr_adj_per[4],
		ddr_settings.wr_adj_per[5],
		ddr_settings.rd_adj_per[0],
		ddr_settings.rd_adj_per[1],
		ddr_settings.rd_adj_per[2],
		ddr_settings.rd_adj_per[3],
		ddr_settings.rd_adj_per[4],
		ddr_settings.rd_adj_per[5]
	);
	printf(
		"\t\t.ddr4_clk               = 0x%x\n" 
		"\t\t.ddr4_drv               = 0x%x\n" 
		"\t\t.ddr4_odt               = 0x%x\n" 

		"\t\t.t_pub_acbdlr3          = 0x%x\n"

		"\t\t.t_pub_soc_vref_dram_vref = 0x%x\n"

		"\t\t.t_pub_soc_vref_dram_vref_rank1 = 0x%x\n"
		"\t\t.wr_adj_per_rank1[0]    = 0x%x\n"
		"\t\t.wr_adj_per_rank1[1]    = 0x%x\n"
		"\t\t.wr_adj_per_rank1[2]    = 0x%x\n"
		"\t\t.wr_adj_per_rank1[3]    = 0x%x\n"
		"\t\t.wr_adj_per_rank1[4]    = 0x%x\n"
		"\t\t.wr_adj_per_rank1[5]    = 0x%x\n"
		"\t\t.rd_adj_per_rank1[0]    = 0x%x\n"
		"\t\t.rd_adj_per_rank1[1]    = 0x%x\n"
		"\t\t.rd_adj_per_rank1[2]    = 0x%x\n"
		"\t\t.rd_adj_per_rank1[3]    = 0x%x\n"
		"\t\t.rd_adj_per_rank1[4]    = 0x%x\n"
		"\t\t.rd_adj_per_rank1[5]    = 0x%x\n"
		"};\n",
		ddr_settings.ddr4_clk, 
		ddr_settings.ddr4_drv, 
		ddr_settings.ddr4_odt, 

		ddr_settings.t_pub_acbdlr3,

		ddr_settings.t_pub_soc_vref_dram_vref,

		ddr_settings.t_pub_soc_vref_dram_vref_rank1,
		ddr_settings.wr_adj_per_rank1[0],
		ddr_settings.wr_adj_per_rank1[1],
		ddr_settings.wr_adj_per_rank1[2],
		ddr_settings.wr_adj_per_rank1[3],
		ddr_settings.wr_adj_per_rank1[4],
		ddr_settings.wr_adj_per_rank1[5],
		ddr_settings.rd_adj_per_rank1[0],
		ddr_settings.rd_adj_per_rank1[1],
		ddr_settings.rd_adj_per_rank1[2],
		ddr_settings.rd_adj_per_rank1[3],
		ddr_settings.rd_adj_per_rank1[4],
		ddr_settings.rd_adj_per_rank1[5]
#endif
	);
}

void print_ddr_timings(ddrt_t ddr_timings[7] __PROBABLY_UNUSED) {
	printf(
		"\n/* \"ddrt_\" structure */"
		"\nddr_timing_t __ddr_timming[] = {\n"
	);

	for (int i = 0; i < 7; i++) {
		printf("\t/* \"ddrt_\" array index: %d */\n", i);
#if ACS_TARGET_GXBB
		printf(
			"\t{\n"
			"\t\t.identifier         = 0x%x,\n\n"
			"\t\t.cfg_ddr_rtp        = 0x%x,\n"
			"\t\t.cfg_ddr_wtr        = 0x%x,\n"
			"\t\t.cfg_ddr_rp         = 0x%x,\n"
			"\t\t.cfg_ddr_rcd        = 0x%x,\n"
			"\t\t.cfg_ddr_ras        = 0x%x,\n"
			"\t\t.cfg_ddr_rrd        = 0x%x,\n"
			"\t\t.cfg_ddr_rc         = 0x%x,\n\n"
			"\t\t.cfg_ddr_mrd        = 0x%x,\n"
			"\t\t.cfg_ddr_mod        = 0x%x,\n"
			"\t\t.cfg_ddr_faw        = 0x%x,\n"
			"\t\t.cfg_ddr_wlmrd      = 0x%x,\n"
			"\t\t.cfg_ddr_wlo        = 0x%x,\n"
			"\t\t.cfg_ddr_rfc        = 0x%x,\n\n"
			"\t\t.cfg_ddr_xp         = 0x%x,\n\n"
			"\t\t.cfg_ddr_xs         = 0x%x,\n"
			"\t\t.cfg_ddr_dllk       = 0x%x,\n"
			"\t\t.cfg_ddr_cke        = 0x%x,\n"
			"\t\t.cfg_ddr_rtodt      = 0x%x,\n"
			"\t\t.cfg_ddr_rtw        = 0x%x,\n\n"
			"\t\t.cfg_ddr_refi       = 0x%x,\n"
			"\t\t.cfg_ddr_refi_mddr3 = 0x%x,\n"
			"\t\t.cfg_ddr_cl         = 0x%x,\n"
			"\t\t.cfg_ddr_wr         = 0x%x,\n"
			"\t\t.cfg_ddr_cwl        = 0x%x,\n"
			"\t\t.cfg_ddr_al         = 0x%x,\n"
			"\t\t.cfg_ddr_dqs        = 0x%x,\n"
			"\t\t.cfg_ddr_cksre      = 0x%x,\n"
			"\t\t.cfg_ddr_cksrx      = 0x%x,\n"
			"\t\t.cfg_ddr_zqcs       = 0x%x,\n"
			"\t\t.cfg_ddr_xpdll      = 0x%x,\n"
			"\t\t.cfg_ddr_exsr       = 0x%x,\n"
			"\t\t.cfg_ddr_zqcl       = 0x%x,\n"
			"\t\t.cfg_ddr_zqcsi      = 0x%x,\n\n"
			"\t\t.cfg_ddr_rpab       = 0x%x,\n"
			"\t\t.cfg_ddr_rppb       = 0x%x,\n"
			"\t\t.cfg_ddr_tdqsck     = 0x%x,\n"
			"\t\t.cfg_ddr_tdqsckmax  = 0x%x,\n"
			"\t\t.cfg_ddr_tckesr     = 0x%x,\n"
			"\t\t.cfg_ddr_tdpd       = 0x%x,\n"
			"\t\t.cfg_ddr_taond_aofd = 0x%x\n"
			"\t},\n",
			ddr_timings[i].identifier,

			ddr_timings[i].cfg_ddr_rtp,
			ddr_timings[i].cfg_ddr_wtr,
			ddr_timings[i].cfg_ddr_rp,
			ddr_timings[i].cfg_ddr_rcd,
			ddr_timings[i].cfg_ddr_ras,
			ddr_timings[i].cfg_ddr_rrd,
			ddr_timings[i].cfg_ddr_rc,

			ddr_timings[i].cfg_ddr_mrd,
			ddr_timings[i].cfg_ddr_mod,
			ddr_timings[i].cfg_ddr_faw,
			ddr_timings[i].cfg_ddr_wlmrd,
			ddr_timings[i].cfg_ddr_wlo,
			ddr_timings[i].cfg_ddr_rfc,

			ddr_timings[i].cfg_ddr_xp,


			ddr_timings[i].cfg_ddr_xs,
			ddr_timings[i].cfg_ddr_dllk,
			ddr_timings[i].cfg_ddr_cke,
			ddr_timings[i].cfg_ddr_rtodt,
			ddr_timings[i].cfg_ddr_rtw,

			ddr_timings[i].cfg_ddr_refi,
			ddr_timings[i].cfg_ddr_refi_mddr3,
			ddr_timings[i].cfg_ddr_cl,
			ddr_timings[i].cfg_ddr_wr,
			ddr_timings[i].cfg_ddr_cwl,
			ddr_timings[i].cfg_ddr_al,
			ddr_timings[i].cfg_ddr_dqs,
			ddr_timings[i].cfg_ddr_cksre,
			ddr_timings[i].cfg_ddr_cksrx,
			ddr_timings[i].cfg_ddr_zqcs,
			ddr_timings[i].cfg_ddr_xpdll,
			ddr_timings[i].cfg_ddr_exsr,
			ddr_timings[i].cfg_ddr_zqcl,
			ddr_timings[i].cfg_ddr_zqcsi,

			ddr_timings[i].cfg_ddr_rpab,
			ddr_timings[i].cfg_ddr_rppb,
			ddr_timings[i].cfg_ddr_tdqsck,
			ddr_timings[i].cfg_ddr_tdqsckmax,
			ddr_timings[i].cfg_ddr_tckesr,
			ddr_timings[i].cfg_ddr_tdpd,
			ddr_timings[i].cfg_ddr_taond_aofd
		);
#elif ACS_TARGET_GXL
		printf(
			"\t{\n"
			"\t\t.identifier         = 0x%x,\n"
			"\t\t.cfg_ddr_rtp        = 0x%x,\n"
			"\t\t.cfg_ddr_wtr        = 0x%x,\n"
			"\t\t.cfg_ddr_rp         = 0x%x,\n"
			"\t\t.cfg_ddr_rcd        = 0x%x,\n"
			"\t\t.cfg_ddr_ras        = 0x%x,\n"
			"\t\t.cfg_ddr_rrd        = 0x%x,\n"
			"\t\t.cfg_ddr_rc         = 0x%x,\n"
			"\t\t.cfg_ddr_mrd        = 0x%x,\n"
			"\t\t.cfg_ddr_mod        = 0x%x,\n"
			"\t\t.cfg_ddr_faw        = 0x%x,\n"
			"\t\t.cfg_ddr_wlmrd      = 0x%x,\n"
			"\t\t.cfg_ddr_wlo        = 0x%x,\n"
			"\t\t.cfg_ddr_xp         = 0x%x,\n"
			"\t\t.cfg_ddr_rfc        = 0x%x,\n"
			"\t\t.cfg_ddr_xs         = 0x%x,\n"
			"\t\t.cfg_ddr_dllk       = 0x%x,\n"
			"\t\t.cfg_ddr_cke        = 0x%x,\n"
			"\t\t.cfg_ddr_rtodt      = 0x%x,\n"
			"\t\t.cfg_ddr_rtw        = 0x%x,\n"

			"\t\t.cfg_ddr_refi       = 0x%x,\n"
			"\t\t.cfg_ddr_refi_mddr3 = 0x%x,\n"
			"\t\t.cfg_ddr_cl         = 0x%x,\n"
			"\t\t.cfg_ddr_wr         = 0x%x,\n"
			"\t\t.cfg_ddr_cwl        = 0x%x,\n"
			"\t\t.cfg_ddr_al         = 0x%x,\n"
			"\t\t.cfg_ddr_dqs        = 0x%x,\n"
			"\t\t.cfg_ddr_cksre      = 0x%x,\n"
			"\t\t.cfg_ddr_cksrx      = 0x%x,\n"
			"\t\t.cfg_ddr_zqcs       = 0x%x,\n"
			"\t\t.cfg_ddr_xpdll      = 0x%x,\n"
			"\t\t.cfg_ddr_exsr       = 0x%x,\n"
			"\t\t.cfg_ddr_zqcl       = 0x%x,\n"
			"\t\t.cfg_ddr_zqcsi      = 0x%x,\n"
			"\t\t.cfg_ddr_tccdl      = 0x%x,\n"
			"\t\t.cfg_ddr_tdqsck     = 0x%x,\n"
			"\t\t.cfg_ddr_tdqsckmax  = 0x%x,\n"
			"\t\t.rsv_char           = 0x%x,\n"
			"\t\t.rsv_int            = 0x%x,\n"
			"\t},\n",
			ddr_timings[i].identifier,

			ddr_timings[i].cfg_ddr_rtp,
			ddr_timings[i].cfg_ddr_wtr,
			ddr_timings[i].cfg_ddr_rp,
			ddr_timings[i].cfg_ddr_rcd,
			ddr_timings[i].cfg_ddr_ras,
			ddr_timings[i].cfg_ddr_rrd,
			ddr_timings[i].cfg_ddr_rc,

			ddr_timings[i].cfg_ddr_mrd,
			ddr_timings[i].cfg_ddr_mod,
			ddr_timings[i].cfg_ddr_faw,
			ddr_timings[i].cfg_ddr_wlmrd,
			ddr_timings[i].cfg_ddr_wlo,

			ddr_timings[i].cfg_ddr_xp,

			ddr_timings[i].cfg_ddr_rfc,

			ddr_timings[i].cfg_ddr_xs,
			ddr_timings[i].cfg_ddr_dllk,
			ddr_timings[i].cfg_ddr_cke,
			ddr_timings[i].cfg_ddr_rtodt,
			ddr_timings[i].cfg_ddr_rtw,

			ddr_timings[i].cfg_ddr_refi,
			ddr_timings[i].cfg_ddr_refi_mddr3,
			ddr_timings[i].cfg_ddr_cl,
			ddr_timings[i].cfg_ddr_wr,
			ddr_timings[i].cfg_ddr_cwl,
			ddr_timings[i].cfg_ddr_al,
			ddr_timings[i].cfg_ddr_dqs,
			ddr_timings[i].cfg_ddr_cksre,
			ddr_timings[i].cfg_ddr_cksrx,
			ddr_timings[i].cfg_ddr_zqcs,
			ddr_timings[i].cfg_ddr_xpdll,
			ddr_timings[i].cfg_ddr_exsr,
			ddr_timings[i].cfg_ddr_zqcl,
			ddr_timings[i].cfg_ddr_zqcsi,

			ddr_timings[i].cfg_ddr_tccdl,
			ddr_timings[i].cfg_ddr_tdqsck,
			ddr_timings[i].cfg_ddr_tdqsckmax,
			ddr_timings[i].rsv_char,

			ddr_timings[i].rsv_int
		);
#endif
	}
	printf("\n};\n");
}

void print_pll_settings(pll_t pll_settings) {
	printf(
		"\n/* \"pll__\" structure */\n"
		"pll_set_t __pll_setting = {\n"
		"\t.cpu_clk         = 0x%x,\n"
		"\t.pxp             = 0x%x,\n"
		"\t.spi_ctrl        = 0x%x,\n"
		"\t.vddee           = 0x%x,\n"
		"\t.vcck            = 0x%x,\n"
		/* ? szPad not specified.. */
		"\t.lCustomerID     = 0x%lx\n"
#if ACS_TARGET_GXL
		"\t.debug_mode      = 0x%x\n"
		"\t.ddr_clk_debug   = 0x%x\n"
		"\t.cpu_clk_debug   = 0x%x\n"
		"\t.rsv_s1          = 0x%x\n"
		"\t.ddr_pll_ssc     = 0x%x\n"
		"\t.rsv_i1          = 0x%x\n"
		"\t.rsv_l3          = 0x%lx\n"
		"\t.rsv_l4          = 0x%lx\n"
		"\t.rsv_l5          = 0x%lx\n"
#endif
		"};\n",
		pll_settings.cpu_clk,
		pll_settings.pxp,
		pll_settings.spi_ctrl,
		pll_settings.vddee,
		pll_settings.vcck,
		pll_settings.customer_id
#if ACS_TARGET_GXL
		, /* Ugh... */
		pll_settings.debug_mode,
		pll_settings.ddr_clk_debug,
		pll_settings.cpu_clk_debug,
		pll_settings.rsv_s1,
		pll_settings.ddr_pll_ssc,
		pll_settings.rsv_i1,
		pll_settings.rsv_l3,
		pll_settings.rsv_l4,
		pll_settings.rsv_l5
#endif
	);
}
