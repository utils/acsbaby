/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (C) 2015, Amlogic, Inc. All rights reserved.
 * Copyright (C) 2023, Ferass El Hafidi <vitali64pmemail@protonmail.com>
 */
#ifndef TIMING_GXBB_H_
#define TIMING_GXBB_H_

typedef struct ddr_settings {
	uint8_t    ddr_channel_set;
	uint8_t    ddr_type;
	uint8_t    ddr_2t_mode;
	uint8_t    ddr_full_test;
	uint8_t    ddr_size_detect;
	uint8_t    ddr_drv;
	uint8_t    ddr_odt;
	uint8_t    ddr_timing_ind;
	uint16_t   ddr_size;
	uint16_t   ddr_clk;
	uint32_t   ddr_base_addr;
	uint32_t   ddr_start_offset;
	uint32_t   ddr_pll_ctrl;
	uint32_t   ddr_dmc_ctrl;
	uint32_t   ddr0_addrmap[5];
	uint32_t   ddr1_addrmap[5];

	uint32_t   t_pub_ptr[5];
	uint16_t   t_pub_mr[4];
	uint32_t   t_pub_odtcr;
	uint32_t   t_pub_dtpr[4];
	uint32_t   t_pub_pgcr0;
	uint32_t   t_pub_pgcr1;
	uint32_t   t_pub_pgcr2;
	uint32_t   t_pub_pgcr3;
	uint32_t   t_pub_dxccr;
	uint32_t   t_pub_dtcr;
	uint32_t   t_pub_aciocr[5];
	uint32_t   t_pub_dx0gcr[3];
	uint32_t   t_pub_dx1gcr[3];
	uint32_t   t_pub_dx2gcr[3];
	uint32_t   t_pub_dx3gcr[3];
	uint32_t   t_pub_dcr;
	uint32_t   t_pub_dtar;
	uint32_t   t_pub_dsgcr;
	uint32_t   t_pub_zq0pr;
	uint32_t   t_pub_zq1pr;
	uint32_t   t_pub_zq2pr;
	uint32_t   t_pub_zq3pr;

	/* PCTL0 */
	uint16_t   t_pctl0_1us_pck;
	uint16_t   t_pctl0_100ns_pck;
	uint16_t   t_pctl0_init_us;
	uint16_t   t_pctl0_rsth_us;
	uint32_t   t_pctl0_mcfg;
	uint32_t   t_pctl0_mcfg1;
	uint16_t   t_pctl0_scfg;
	uint16_t   t_pctl0_sctl;
	uint32_t   t_pctl0_ppcfg;
	uint16_t   t_pctl0_dfistcfg0;
	uint16_t   t_pctl0_dfistcfg1;
	uint16_t   t_pctl0_dfitctrldelay;
	uint16_t   t_pctl0_dfitphywrdata;
	uint16_t   t_pctl0_dfitphywrlta;
	uint16_t   t_pctl0_dfitrddataen;
	uint16_t   t_pctl0_dfitphyrdlat;
	uint16_t   t_pctl0_dfitdramclkdis;
	uint16_t   t_pctl0_dfitdramclken;
	uint16_t   t_pctl0_dfitphyupdtype1;
	uint16_t   t_pctl0_dfitctrlupdmin;
	uint16_t   t_pctl0_cmdtstaten;
	uint32_t   t_pctl0_dfiodtcfg;
	uint32_t   t_pctl0_dfiodtcfg1;
	uint32_t   t_pctl0_dfilpcfg0;

	uint32_t   t_pub_acbdlr0;

	/*
	 * Version 2 of the ddrs_ entry adds the following
	 */
	uint32_t   ddr_func;
	uint8_t    wr_adj_per[6];
	uint8_t    rd_adj_per[6];

	/*
	 * Version 3 of the ddrs_ entry adds the following
	 */
	uint16_t t_pub_mr11;
	uint8_t  t_lpddr3_ca0;
	uint8_t  t_lpddr3_ca1;
	uint8_t  t_lpddr3_remap;
	uint8_t  t_lpddr3_wl;
	uint8_t  rsv1;
	uint8_t  rsv2;

	/* aligned 4 bytes */
} __attribute__ ((packed)) ddrs_t;

typedef struct ddr_timings {
	//Identifier
	uint8_t  identifier;

	//DTPR0
	uint8_t  cfg_ddr_rtp;
	uint8_t  cfg_ddr_wtr;
	uint8_t  cfg_ddr_rp;
	uint8_t  cfg_ddr_rcd;
	uint8_t  cfg_ddr_ras;
	uint8_t  cfg_ddr_rrd;
	uint8_t  cfg_ddr_rc;

	//DTPR1
	uint8_t  cfg_ddr_mrd;
	uint8_t  cfg_ddr_mod;
	uint8_t  cfg_ddr_faw;
	uint8_t  cfg_ddr_wlmrd;
	uint8_t  cfg_ddr_wlo;

	//DTPR2
	uint8_t  cfg_ddr_xp;

	//DTPR1
	uint16_t cfg_ddr_rfc;

	//DTPR2
	uint16_t cfg_ddr_xs;
	uint16_t cfg_ddr_dllk;
	uint8_t  cfg_ddr_cke;
	uint8_t  cfg_ddr_rtodt;
	uint8_t  cfg_ddr_rtw;

	uint8_t  cfg_ddr_refi;
	uint8_t  cfg_ddr_refi_mddr3;
	uint8_t  cfg_ddr_cl;
	uint8_t  cfg_ddr_wr;
	uint8_t  cfg_ddr_cwl;
	uint8_t  cfg_ddr_al;
	uint8_t  cfg_ddr_dqs;
	uint8_t  cfg_ddr_cksre;
	uint8_t  cfg_ddr_cksrx;
	uint8_t  cfg_ddr_zqcs;
	uint8_t  cfg_ddr_xpdll;
	uint16_t cfg_ddr_exsr;
	uint16_t cfg_ddr_zqcl;
	uint16_t cfg_ddr_zqcsi;

	/* timing v2 added, lpddr3 support */
	uint16_t cfg_ddr_rpab;
	uint16_t cfg_ddr_rppb;
	uint16_t cfg_ddr_tdqsck;
	uint16_t cfg_ddr_tdqsckmax;
	uint16_t cfg_ddr_tckesr;
	uint16_t cfg_ddr_tdpd;
	uint16_t cfg_ddr_taond_aofd;
} __attribute__ ((packed)) ddrt_t;
#endif
