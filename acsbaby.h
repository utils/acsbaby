/* SPDX-License-Identifier: GPL-3.0-or-later */
/*
 * Copyright (C) 2023, Ferass El Hafidi <vitali64pmemail@protonmail.com>
 */
#ifndef ACSBABY_H
#define ACSBABY_H
#include <stdint.h>

typedef struct acs_entry {
	uint8_t  version;
	uint16_t size;
	uint32_t address;
} acs_entry_t;

/* ACS footer */
typedef struct acs_struct {
	uint8_t     chip_type;
	uint8_t     version;
	uint16_t    size;
	/* Entries */
	acs_entry_t ddrs; /* DDR settings */
	acs_entry_t ddrt; /* DDR timings */
	acs_entry_t pll;  /* PLL settings */
} acs_t;

#endif
